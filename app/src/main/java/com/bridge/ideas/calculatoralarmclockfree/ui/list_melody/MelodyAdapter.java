package com.bridge.ideas.calculatoralarmclockfree.ui.list_melody;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BaseRecyclerAdapter;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MelodyAdapter extends BaseRecyclerAdapter {
    private List<Melody> mMelodies;
    private IClickListener listener;
    private MediaPlayer mPlayer = new MediaPlayer();
    private boolean isPlaying;
    private int currentPosition = -1;
    private int previousPosition = -1;


    MelodyAdapter(List<Melody> melodies) {
        mMelodies = melodies;
    }

    @Override
    public int getItemCount() {
        return mMelodies.size();
    }

    @Override
    public Melody getItem(int position) {
        return mMelodies.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MelodyAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_melody, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((MelodyAdapter.ViewHolder) holder).bindView(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title) TextView title;
        @BindView(R.id.playPauseButton) ImageView playPauseButton;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        void bindView(int position) {
            Melody melody = getItem(position);
            title.setText(melody.getName());
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (position != currentPosition) {
                        previousPosition = currentPosition;
                        currentPosition = position;
                        isPlaying = false;
                        mPlayer.stop();
                        notifyItemChanged(previousPosition);
                        notifyItemChanged(currentPosition);
                    }
                   /* isPlaying = false;
                    mPlayer.stop();
                    listener.onSelected(melody);*/
                }
            });

            if (currentPosition == position) {
                playPauseButton.setImageResource(isPlaying ? R.drawable.pause_purple : R.drawable.play_purple);
            } else {
                playPauseButton.setImageResource(R.drawable.play_purple);
            }
            title.setTextColor(currentPosition == position ? title.getContext().getResources().getColor(R.color.active_music) : title.getContext().getResources().getColor(R.color.black));

            playPauseButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isPlaying && currentPosition == position) {
                        mPlayer.stop();
                        isPlaying = false;

                    } else if (isPlaying) {
                        mPlayer.stop();
                        initPlayer(melody);
                        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                isPlaying = false;
                                notifyItemChanged(position);
                            }
                        });
                        mPlayer.start();
                    } else {
                        initPlayer(melody);
                        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                isPlaying = false;
                                notifyItemChanged(position);
                            }
                        });
                        mPlayer.start();
                        isPlaying = true;
                    }
                    previousPosition = currentPosition;
                    currentPosition = position;
                    notifyItemChanged(previousPosition);
                    notifyItemChanged(currentPosition);
                }
            });
        }

        void initPlayer(Melody melody){
            if(melody.getRecourseId() == null){
                mPlayer = new MediaPlayer();
                try {
                    mPlayer.setDataSource(playPauseButton.getContext(), Uri.parse(melody.getUri()));
                } catch (Exception e) {

                }
                try {
                    mPlayer.prepare();
                } catch (Exception e) {
                    Toast.makeText(App.getInstance().getApplicationContext(), R.string.melody_broken, Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            } else {
                mPlayer = MediaPlayer.create(playPauseButton.getContext(), melody.getRecourseId());
            }

            if(mPlayer == null){
                mPlayer = MediaPlayer.create(playPauseButton.getContext(), R.raw.today_will_be_a_holiday);
            }
        }

    }

    public void setListener(IClickListener listener) {
        this.listener = listener;
    }

    public interface IClickListener {
        void onSelected(Melody melody);
    }

    void stopMusic() {
        isPlaying = false;
        notifyDataSetChanged();
        previousPosition = -1;
        currentPosition = -1;
        mPlayer.stop();
    }

    Melody getSelectedMelody() {
        if (currentPosition == -1) {
            return null;
        }
        return mMelodies.get(currentPosition);
    }
}
