package com.bridge.ideas.calculatoralarmclockfree.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Alarm implements Parcelable {
    public static final String TAG = "alarm";
    @SerializedName("id")
    private Long id;
    @SerializedName("time")
    private Long time;
    @SerializedName("repeat")
    private List<Integer> repeat;
    @SerializedName("melody")
    private Melody melody;
    @SerializedName("comment")
    private String comment;
    @SerializedName("enabled")
    private Boolean enabled;
    @Expose(serialize = false)
    private Boolean checked;

    public Alarm() {
    }

    protected Alarm(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readLong();
        }
        if (in.readByte() == 0) {
            time = null;
        } else {
            time = in.readLong();
        }
        melody = in.readParcelable(Melody.class.getClassLoader());
        comment = in.readString();
        byte tmpEnabled = in.readByte();
        enabled = tmpEnabled == 0 ? null : tmpEnabled == 1;
        byte tmpChecked = in.readByte();
        checked = tmpChecked == 0 ? null : tmpChecked == 1;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(id);
        }
        if (time == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(time);
        }
        dest.writeParcelable(melody, flags);
        dest.writeString(comment);
        dest.writeByte((byte) (enabled == null ? 0 : enabled ? 1 : 2));
        dest.writeByte((byte) (checked == null ? 0 : checked ? 1 : 2));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Alarm> CREATOR = new Creator<Alarm>() {
        @Override
        public Alarm createFromParcel(Parcel in) {
            return new Alarm(in);
        }

        @Override
        public Alarm[] newArray(int size) {
            return new Alarm[size];
        }
    };

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Boolean getChecked() {
        if(checked == null){
            return false;
        }
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public List<Integer> getRepeat() {
        return repeat;
    }

    public void setRepeat(List<Integer> repeat) {
        this.repeat = repeat;
    }

    public Melody getMelody() {
        return melody;
    }

    public void setMelody(Melody melody) {
        this.melody = melody;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Alarm alarm = (Alarm) o;
        return id.equals(alarm.id);
    }

    @Override
    public int hashCode() {
        return 0;
    }
}
