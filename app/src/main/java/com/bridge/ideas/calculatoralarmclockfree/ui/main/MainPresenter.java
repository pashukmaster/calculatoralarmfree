package com.bridge.ideas.calculatoralarmclockfree.ui.main;

import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.arellomobile.mvp.InjectViewState;
import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.base.BasePresenter;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.helper.FileHelper;
import com.bridge.ideas.calculatoralarmclockfree.models.Melodies;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;

import java.io.File;

import javax.inject.Inject;

@InjectViewState
public class MainPresenter extends BasePresenter<MainView> {

    @Inject LocalStorage localStorage;
    public static final String DELAY = "delay";
    public static final String VIBRATION = "vibration";
    public static final String MELODY_LIST = "MELODY_LIST";
    public static final String PUSH_BUTTON_COLOR = "PUSH_BUTTON_COLOR";
    public static final String ALARM_VOLUME = "ALARM_VOLUME";


    MainPresenter() {
        App.getAppComponent().inject(this);
    }


    void changeVibration(boolean status) {
        localStorage.writeBoolean(VIBRATION, status);
        getViewState().showVibration(status);
    }

    void changeDelay(boolean status) {
        localStorage.writeBoolean(DELAY, status);
        getViewState().showDelay(status);
    }

    void setColorNum(int colorNum) {
        localStorage.writeInteger(PUSH_BUTTON_COLOR, colorNum);
        getViewState().showColorNumber(colorNum);
    }

    public Integer getColorNum() {
        Integer pushedColor = localStorage.readInteger(PUSH_BUTTON_COLOR);
        return pushedColor;
    }

    void setAlarmVolume(int alarmVolume) {
        localStorage.writeInteger(ALARM_VOLUME, alarmVolume);
    }

    public Integer getAlarmVolume() {
        Integer alarmVolume = localStorage.readInteger(ALARM_VOLUME);
        return alarmVolume;
    }


    void loadData() {
        getViewState().showVibration(localStorage.readBoolean(VIBRATION, true));
        getViewState().showDelay(localStorage.readBoolean(DELAY, true));
        getViewState().showColorNumber(localStorage.readInteger(PUSH_BUTTON_COLOR));
    }

    void addMelody(Uri uri) {
        Melodies melodies = localStorage.readObject(MELODY_LIST, Melodies.class);
        if (melodies == null) {
            melodies = new Melodies();
        }
        File file = new File(FileHelper.getRealPathFromURI(App.getInstance().getApplicationContext(), uri));
        String fileName = file.getName();
        if (fileName.indexOf(".") > 0)
            fileName = fileName.substring(0, fileName.lastIndexOf("."));
        Melody melody = new Melody(null, fileName, uri.toString());
        melodies.getMelodyList().add(melody);

        localStorage.writeObject(MELODY_LIST, melodies);
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = App.getInstance().getApplicationContext().getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }
}
