package com.bridge.ideas.calculatoralarmclockfree.dagger.module;

import android.content.Context;

import com.google.gson.Gson;

public class LocalStorage {

    private static final String SETTINGS = "settings";
    private Context context;

    public LocalStorage(Context context) {
        this.context = context;
    }

    public void write(String key, String value) {
        if(value == null){
            context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                    .edit().remove(key).apply();
        }else {
            context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                    .edit().putString(key, value).apply();
        }
    }

    public String read(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getString(key, "");
    }

    public void writeLong(String key, Long value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putLong(key, value).apply();
    }

    public Long readLong(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getLong(key, 0);
    }

    public void writeInteger(String key, Integer value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putInt(key, value).apply();
    }

    public Integer readInteger(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getInt(key, 0);
    }

    public void writeFloat(String key, Float value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putFloat(key, value).apply();
    }

    public Float readFloat(String key) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getFloat(key, 0);
    }

    public void writeBoolean(String key, Boolean value) {
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .edit().putBoolean(key, value).apply();
    }

    public Boolean readBoolean(String key, boolean defaultValue) {
        return context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE)
                .getBoolean(key, defaultValue);
    }
    public void writeObject(String key, Object object) {
        Gson gson = new Gson();
        String json = gson.toJson(object);
        context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).edit().putString(key, json).apply();
    }

    public <T> T readObject(String key, Class<T> type) {
        Gson gson = new Gson();
        String json = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).getString(key, "");
        Object fromJson = gson.fromJson(json, type);
        return (T) fromJson;
    }
}
