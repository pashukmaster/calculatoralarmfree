package com.bridge.ideas.calculatoralarmclockfree.ui.list_melody;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BaseFragment;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;
import com.bridge.ideas.calculatoralarmclockfree.ui.main.MainActivity;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MusicListFragment extends BaseFragment implements MusicListView {


    @BindView(R.id.list) RecyclerView list;
    MelodyAdapter melodyAdapter;
    @InjectPresenter MusicListPresenter mPresenter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();
    }

    private void initRecyclerView() {
        list.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);
    }

    @Override
    public void showMelodyList(List<Melody> melodyList) {
        melodyAdapter = new MelodyAdapter(melodyList);
        melodyAdapter.setListener(new MelodyAdapter.IClickListener() {
            @Override
            public void onSelected(Melody melody) {
                ((MainActivity)getActivity()).setMelody(melody);
                getActivity().onBackPressed();
            }
        });
        list.setAdapter(melodyAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            melodyAdapter.stopMusic();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        melodyAdapter.stopMusic();
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.loadMelodyList();
    }

    @OnClick(R.id.download)
    protected void onClickDownload(){
        Log.d("PashaMelody", "clickDownload");
        if(melodyAdapter.getSelectedMelody() != null) {
            Log.d("PashaMelody", "setMelody");
            Log.d("PashaMelody", melodyAdapter.getSelectedMelody().toString());
            ((MainActivity) getActivity()).setMelody(melodyAdapter.getSelectedMelody());
        }
        getActivity().onBackPressed();
    }
}
