package com.bridge.ideas.calculatoralarmclockfree.base;

import android.annotation.SuppressLint;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatFragment;

import butterknife.Unbinder;

public class BaseFragment extends MvpAppCompatFragment implements BaseView {
    public static final String TAG = "myLog";

    protected View progress;
    protected Unbinder unbinder;

    @SuppressLint("ResourceType")
    public void showToast(@IdRes int id) {
        Toast.makeText(getActivity(), id, Toast.LENGTH_SHORT).show();
    }

    public void showToast(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress(boolean status) {
        if(progress != null){
            progress.setVisibility(status ? View.VISIBLE : View.GONE);
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}
