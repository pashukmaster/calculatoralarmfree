package com.bridge.ideas.calculatoralarmclockfree.ui.main;

import com.bridge.ideas.calculatoralarmclockfree.base.BaseView;

public interface MainView extends BaseView {
    void showVibration(boolean status);
    void showColorNumber(int colorNum);
    void showDelay(boolean status);
}
