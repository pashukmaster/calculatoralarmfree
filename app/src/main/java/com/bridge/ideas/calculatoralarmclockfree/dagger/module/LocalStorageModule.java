package com.bridge.ideas.calculatoralarmclockfree.dagger.module;


import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ContextModule.class})
public class LocalStorageModule {

    @Provides
    @Singleton
    public LocalStorage provideLocalStorage(Context context){
        return new LocalStorage(context);
    }
}
