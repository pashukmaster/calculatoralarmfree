package com.bridge.ideas.calculatoralarmclockfree.ui.alarm;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BaseActivity;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.helper.HelperAlarm;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;
import com.bridge.ideas.calculatoralarmclockfree.models.Parcelables;
import com.bridge.ideas.calculatoralarmclockfree.ui.calculator.CalculatorFragment;
import com.bridge.ideas.calculatoralarmclockfree.ui.main.MainActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AlarmActivity extends BaseActivity implements AlarmView {

    MediaPlayer mediaPlayer;
    Vibrator vibrator;



    static boolean setOffForOnceAlarm = false;

    @BindView(R.id.minutes5) View minutes5;
    @BindView(R.id.minutes10) View minutes10;
    @BindView(R.id.minutes15) View minutes15;
    @BindView(R.id.minutes20) View minutes20;
    @BindView(R.id.alarm_comment) TextView alarmComment;

    @InjectPresenter AlarmPresenter mPresenter;

    Alarm alarm;

    @Inject
    LocalStorage localStorage;
    List<Alarm> alarmList;

    CalculatorFragment calculatorFragment = new CalculatorFragment();
    private static final String FRAGMENT_CALCULATOR = "CalculatorFragment";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        unbinder = ButterKnife.bind(this);

        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


        final Bundle bundle = getIntent().getExtras();

        alarm = Parcelables.toParcelableAlarm(bundle.getByteArray(Alarm.TAG));
        String alarmCommentText = alarm.getComment();
        if(alarmCommentText != null) {
            alarmComment.setText(alarmCommentText);
        }
        //volumeSeekbar.getProgress();
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        if(mPresenter.getAlarmVolime() != null && mPresenter.getAlarmVolime() > 0) {
            am.setStreamVolume(AudioManager.STREAM_MUSIC,
                    mPresenter.getAlarmVolime(), 0);
        } else {
            am.setStreamVolume(AudioManager.STREAM_MUSIC, am.getStreamMaxVolume(AudioManager.STREAM_MUSIC), 0);
        }

        initPlayer(alarm.getMelody());
        mediaPlayer.start();

        mPresenter.loadAlarm();
        mPresenter.removeOnceAlarmWhichAccured(alarm);
    }




    @SuppressLint("ResourceType")
    void initPlayer(Melody melody){
        if(melody.getRecourseId() == null){
            mediaPlayer = new MediaPlayer();
            try {
                mediaPlayer.setDataSource(this, Uri.parse(melody.getUri()));
            } catch (IOException e) {

            }
            try {
                mediaPlayer.prepare();
            } catch (IOException e) {
                showToast(R.string.melody_broken);
                e.printStackTrace();
            }
        } else {
            mediaPlayer = MediaPlayer.create(this, melody.getRecourseId());
        }

        if(mediaPlayer == null){
            mediaPlayer = MediaPlayer.create(this, R.raw.today_will_be_a_holiday);
        }
    }
    @Override
    public void onAttachedToWindow() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }


    @OnClick(R.id.turn)
    protected void clickStop(View view) {
        finish();
    }


    public void changeFragment(Fragment fragment, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        if (fm.findFragmentByTag(FRAGMENT_CALCULATOR) != null) {
            ft.hide(fm.findFragmentByTag(FRAGMENT_CALCULATOR));
        }

        Fragment fragmentOld = fm.findFragmentByTag(tag);
        if (fragmentOld != null) {
            fragment = fragmentOld;
            ft.show(fragment).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mediaPlayer.stop();
        vibrator.cancel();
    }

    @Override
    public void showDelay() {
        minutes5.setVisibility(View.VISIBLE);
        minutes20.setVisibility(View.VISIBLE);
        minutes15.setVisibility(View.VISIBLE);
        minutes10.setVisibility(View.VISIBLE);
    }

    @Override
    public void vibrate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createWaveform(new long[]{0, 400, 1000, 600, 1000, 800, 1000, 1000}, 1));
        } else {
            vibrator.vibrate(new long[]{0, 400, 1000, 600, 1000, 800, 1000, 1000}, 1);
        }
    }

    @OnClick({R.id.minutes5, R.id.minutes10, R.id.minutes15, R.id.minutes20})
    protected void clickWait(View view){
        switch (view.getId()){
            case R.id.minutes5:
                waitAlarm(300000);
                break;
            case R.id.minutes10:
                waitAlarm(600000);
                break;
            case R.id.minutes15:
                waitAlarm(900000);
                break;
            case R.id.minutes20:
                waitAlarm(1200000);
                break;
        }
    }

    protected void waitAlarm(long delay){
        Alarm waitAlarm = new Alarm();
        waitAlarm.setMelody(alarm.getMelody());
        alarm.setId(System.currentTimeMillis());
        alarm.setTime(System.currentTimeMillis() + delay);
        alarm.setRepeat(new ArrayList<>());
        alarm.setEnabled(true);
        HelperAlarm.updateAlarm(alarm, true);
        finish();
    }
}
