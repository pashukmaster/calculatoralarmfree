package com.bridge.ideas.calculatoralarmclockfree.ui.list;

import com.bridge.ideas.calculatoralarmclockfree.base.BaseView;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;

import java.util.List;

public interface AlarmListView extends BaseView {
    void showAlarmList(List<Alarm> alarmList);
    void editAlarm(Alarm alarm);
}
