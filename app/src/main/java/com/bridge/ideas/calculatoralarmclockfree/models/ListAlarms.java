package com.bridge.ideas.calculatoralarmclockfree.models;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class ListAlarms implements Comparator<Alarm> {
    @SerializedName("alarm_list")
    List<Alarm> alarmList;

    @Override
    public int compare(Alarm a1, Alarm a2) {
        return a1.getTime().compareTo(a2.getTime());
    }

    public List<Alarm> getAlarmList() {
        if (alarmList == null) {
            alarmList = new ArrayList<>();
        }
        return alarmList;
    }

    public List<Alarm>  bigConvert(List<Alarm> listAlarm) {

        List<Alarm> alarmEnabledList = new ArrayList<Alarm>();



        for (Iterator<Alarm> i = listAlarm.iterator(); i.hasNext(); ) {
            Alarm item = i.next();
                alarmEnabledList.add(item);
        }
        return alarmEnabledList;
    }

    public int getClosestAlarmWhichIsOnInMins(List<Alarm> listAlarm) {

        Collections.sort(listAlarm, new ListAlarms());
        List<Long> listOfTimes = new ArrayList<Long>();
        List<Integer> list = new ArrayList<Integer>();
        List<Alarm> alarmEnabledList = new ArrayList<Alarm>();

        Calendar calCurrent = Calendar.getInstance();
        int currentDay = calCurrent.get(Calendar.DAY_OF_WEEK);
        int currentHour = calCurrent.get(Calendar.HOUR_OF_DAY);
        int currentMinute = calCurrent.get(Calendar.MINUTE);
        int fullCurrentDayMinutes = currentHour * 60 + currentMinute;

        for (Iterator<Alarm> i = listAlarm.iterator(); i.hasNext(); ) {
            Alarm item = i.next();
            if (item.getEnabled() == true) {
                alarmEnabledList.add(item);
            }
        }


        for (Iterator<Alarm> i = alarmEnabledList.iterator(); i.hasNext(); ) {
            Alarm item = i.next();

            Log.d("PashaTime", item.getTime().toString());

            listOfTimes.add(item.getTime());

            int timeToAlarm = 0;

            List<Integer> repeats = new ArrayList<Integer>();

            long timeInList = item.getTime();//.get(k);//1540151133304L;

            Calendar calendar = Calendar.getInstance();

            calendar.setTimeInMillis(timeInList);

            int setDay = calendar.get(Calendar.DAY_OF_WEEK);
            int setHour = calendar.get(Calendar.HOUR_OF_DAY);
            int setMinute = calendar.get(Calendar.MINUTE);
            int setFullCurrentDayMinutes = setHour * 60 + setMinute;
            if (!item.getRepeat().isEmpty()) {
                Log.d("PashaTimeRepeat", item.getRepeat().toString());

                for (Iterator<Integer> j = item.getRepeat().iterator(); j.hasNext(); ) {
                    Integer dayOfTheWeek = j.next();
                    //4     //3
                    if (dayOfTheWeek <= currentDay) { //7 - currentDay + dayOfTheWeek
                        if (dayOfTheWeek == currentDay) {
                            if (setFullCurrentDayMinutes < fullCurrentDayMinutes) {
                                timeToAlarm = 6 * 1440 + setFullCurrentDayMinutes;
                            } else {
                                timeToAlarm = setFullCurrentDayMinutes;
                            }
                        } else {
                            timeToAlarm = (7 - currentDay + dayOfTheWeek) * 1440 + setFullCurrentDayMinutes;
                        }
                    } else {
                        timeToAlarm = dayOfTheWeek * 1440 + setFullCurrentDayMinutes;
                    }
                    list.add(timeToAlarm);
                }
            } else {
                if (setFullCurrentDayMinutes < fullCurrentDayMinutes) {
                    if(currentDay > 6) {
                        currentDay = 0;
                    }
                    timeToAlarm = (currentDay+1)*1440 + setFullCurrentDayMinutes;
                } else {
                    timeToAlarm = currentDay * 1440 + setFullCurrentDayMinutes;
                }
                list.add(timeToAlarm);
            }
        }

        Integer closestTimeInMins = 0;
        if (!list.isEmpty()) {
            Collections.sort(list);
            for (Integer log : list) {
                Log.d("Pasha", log.toString());
            }
            closestTimeInMins = list.get(0);
        } else {
            closestTimeInMins = -1;
        }
        Log.d("PashaTimeClosest", closestTimeInMins.toString());

        return closestTimeInMins;
    }

    public void setAlarmList(List<Alarm> alarmList) {
        this.alarmList = alarmList;
    }

    public void removeAlarm(Alarm alarm) {
        int index = getAlarmList().indexOf(alarm);
        getAlarmList().remove(index);
    }

}
