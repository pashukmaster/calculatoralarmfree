package com.bridge.ideas.calculatoralarmclockfree.ui.list_melody;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BasePresenter;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.models.Melodies;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.bridge.ideas.calculatoralarmclockfree.ui.main.MainPresenter.MELODY_LIST;

@InjectViewState
public class MusicListPresenter extends BasePresenter<MusicListView> {


    @Inject LocalStorage localStorage;

    MusicListPresenter() {
        App.getAppComponent().inject(this);
    }

    void loadMelodyList(){
        List<Melody> melodyList  = new ArrayList<>();
        melodyList.add(new Melody(R.raw.today_will_be_a_holiday, "Today will be a holiday", null));
        melodyList.add(new Melody(R.raw.another_day, "Another day", null));
        melodyList.add(new Melody(R.raw.brutal_morning, "Brutal morning", null));
        melodyList.add(new Melody(R.raw.classical_morning, "Classical morning", null));
        melodyList.add(new Melody(R.raw.french_morning, "French morning", null));
        melodyList.add(new Melody(R.raw.growing_storm, "Growing storm", null));
        melodyList.add(new Melody(R.raw.morning_of_the_day, "Morning of the day", null));
        melodyList.add(new Melody(R.raw.puritan_morning, "Puritan morning", null));
        melodyList.add(new Melody(R.raw.slow_awaking, "Slow awaking", null));

        Melodies melodies = localStorage.readObject(MELODY_LIST, Melodies.class);
        if(melodies != null){
            melodyList.addAll(melodies.getMelodyList());
        }
        Log.d("Melody", String.valueOf(melodyList));
        getViewState().showMelodyList(melodyList);
    }
}
