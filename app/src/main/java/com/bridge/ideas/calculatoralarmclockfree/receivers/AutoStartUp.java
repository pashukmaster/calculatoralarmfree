package com.bridge.ideas.calculatoralarmclockfree.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bridge.ideas.calculatoralarmclockfree.helper.HelperAlarm;
import com.bridge.ideas.calculatoralarmclockfree.models.ListAlarms;
import com.google.gson.Gson;

public class AutoStartUp extends BroadcastReceiver {

    private static final String SETTINGS = "settings";
    public static final String LIST_ALARMS = "alarm_list";


    @Override
    public void onReceive(Context context, Intent intent) {
        Gson gson = new Gson();
        String json = context.getSharedPreferences(SETTINGS, Context.MODE_PRIVATE).getString(LIST_ALARMS, "");
        Object fromJson = gson.fromJson(json, ListAlarms.class);
        ListAlarms listAlarms = (ListAlarms) fromJson;
        for(int i=0; i < listAlarms.getAlarmList().size(); i++){
            //Log.w("myLog", "onReceive: register after reboot" );
            HelperAlarm.updateAlarm(listAlarms.getAlarmList().get(i), listAlarms.getAlarmList().get(i).getEnabled());
        }
    }
}
