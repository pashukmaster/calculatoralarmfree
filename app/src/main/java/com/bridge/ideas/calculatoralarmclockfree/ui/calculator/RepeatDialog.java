package com.bridge.ideas.calculatoralarmclockfree.ui.calculator;


import android.app.Dialog;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.views.DaySwitcher;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RepeatDialog extends DialogFragment {

    @BindView(R.id.mon) DaySwitcher mon;
    @BindView(R.id.tue) DaySwitcher tue;
    @BindView(R.id.wed) DaySwitcher wed;
    @BindView(R.id.thu) DaySwitcher thu;
    @BindView(R.id.fri) DaySwitcher fri;
    @BindView(R.id.sut) DaySwitcher sut;
    @BindView(R.id.sun) DaySwitcher sun;

    IChooseListener listener;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_repeat, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public void setListener(IChooseListener listener) {
        this.listener = listener;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getActivity() != null) {
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = (size.x);
            int height = (size.y);

            Dialog dialog = getDialog();
            if (dialog != null && dialog.getWindow() != null) {
                dialog.getWindow().setLayout(width, height);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            }
        }

    }

    @OnClick(R.id.weekdays)
    protected void onClickWeekdays() {
        mon.setChecked(true);
        tue.setChecked(true);
        wed.setChecked(true);
        thu.setChecked(true);
        fri.setChecked(true);
        sut.setChecked(false);
        sun.setChecked(false);
    }

    @OnClick(R.id.everyday)
    protected void onClickEveryday() {
        mon.setChecked(true);
        tue.setChecked(true);
        wed.setChecked(true);
        thu.setChecked(true);
        fri.setChecked(true);
        sut.setChecked(true);
        sun.setChecked(true);
    }


    @OnClick(R.id.once)
    protected void onClickOnce() {
        mon.setChecked(false);
        tue.setChecked(false);
        wed.setChecked(false);
        thu.setChecked(false);
        fri.setChecked(false);
        sut.setChecked(false);
        sun.setChecked(false);
    }


    @OnClick(R.id.choose)
    protected void clickChoose() {
        dismiss();
        List<Integer> repeat = new ArrayList<>();
        if (mon.isChecked()) {
            repeat.add(Calendar.MONDAY);
        }
        if (tue.isChecked()) {
            repeat.add(Calendar.TUESDAY);
        }
        if (wed.isChecked()) {
            repeat.add(Calendar.WEDNESDAY);
        }
        if (thu.isChecked()) {
            repeat.add(Calendar.THURSDAY);
        }
        if (fri.isChecked()) {
            repeat.add(Calendar.FRIDAY);
        }
        if (sut.isChecked()) {
            repeat.add(Calendar.SATURDAY);
        }
        if (sun.isChecked()) {
            repeat.add(Calendar.SUNDAY);
        }

        listener.onChoose(repeat);
    }

    public interface IChooseListener {
        void onChoose(List<Integer> repeats);
    }
}
