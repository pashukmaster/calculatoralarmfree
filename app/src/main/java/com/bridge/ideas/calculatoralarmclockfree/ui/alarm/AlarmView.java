package com.bridge.ideas.calculatoralarmclockfree.ui.alarm;

import com.bridge.ideas.calculatoralarmclockfree.base.BaseView;

public interface AlarmView extends BaseView {

    void showDelay();

    void vibrate();

}
