package com.bridge.ideas.calculatoralarmclockfree.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bridge.ideas.calculatoralarmclockfree.R;

public class DaySwitcher extends LinearLayout {

    TextView bottomText;
    TextView topText;
    View track;
    View trackBackground;
    View switcher;
    boolean checked = false;
    String day = "Mon";

    ICheckChangeListener listener;

    public DaySwitcher(Context context) {
        super(context);
        init(context);
    }

    public DaySwitcher(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.DaySwitcher, 0, 0);
        try {
            day = ta.getString(R.styleable.DaySwitcher_day);
        } finally {
            ta.recycle();
        }
        init(context);
    }

    public DaySwitcher(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.DaySwitcher, 0, 0);
        try {
            day = ta.getString(R.styleable.DaySwitcher_day);
        } finally {
            ta.recycle();
        }
        init(context);
    }

    private void init(Context context) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_day_switcher, this);

        bottomText = view.findViewById(R.id.bottomText);
        bottomText.setText(day);
        topText = view.findViewById(R.id.topText);
        topText.setText(day);
        track = view.findViewById(R.id.track);
        trackBackground = view.findViewById(R.id.trackBackground);

        switcher = view.findViewById(R.id.switcher);
        switcher.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                checked = !checked;
                if(listener != null){
                    listener.onChange(checked);
                }
                loadView();
            }
        });
        loadView();
    }

    public void setChecked(boolean checked) {
        if(this.checked == checked){
            return;
        }
        this.checked = checked;
        loadView();
    }

    public boolean isChecked() {
        return checked;
    }

    private void loadView(){
        if(track == null){
            return;
        }
        Animation alphaTrack = AnimationUtils.loadAnimation(getContext(), R.anim.show);
        trackBackground.setBackgroundResource(!checked ? R.drawable.track_day_on : R.drawable.track_day_off);
        track.setBackgroundResource(checked ? R.drawable.track_day_on : R.drawable.track_day_off);
        track.startAnimation(alphaTrack);


        Animation translate = AnimationUtils.loadAnimation(getContext(), checked ? R.anim.translate_top : R.anim.translate_bottom);
        translate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                topText.setVisibility(checked ? View.VISIBLE : GONE);
                bottomText.setVisibility(!checked ? View.VISIBLE : GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        bottomText.startAnimation(translate);
        Animation alphaThumb = AnimationUtils.loadAnimation(getContext(), checked ? R.anim.hide : R.anim.show);

        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(translate);
        animationSet.addAnimation(alphaThumb);
        bottomText.startAnimation(animationSet);
    }

    public interface ICheckChangeListener {
        void onChange(boolean status);
    }
}
