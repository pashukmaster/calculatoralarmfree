package com.bridge.ideas.calculatoralarmclockfree.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Melody implements Parcelable {
    @SerializedName("recourseId")
    private Integer recourseId;
    @SerializedName("name")
    private String name;
    @SerializedName("uri")
    private String uri;

    public Melody(Integer recourseId, String name, String uri) {
        this.recourseId = recourseId;
        this.name = name;
        this.uri = uri;
    }

    public Integer getRecourseId() {
        return recourseId;
    }

    public void setRecourseId(Integer recourseId) {
        this.recourseId = recourseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    protected Melody(Parcel in) {
        if (in.readByte() == 0) {
            recourseId = null;
        } else {
            recourseId = in.readInt();
        }
        name = in.readString();
        uri = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (recourseId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(recourseId);
        }
        dest.writeString(name);
        dest.writeString(uri);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Melody> CREATOR = new Creator<Melody>() {
        @Override
        public Melody createFromParcel(Parcel in) {
            return new Melody(in);
        }

        @Override
        public Melody[] newArray(int size) {
            return new Melody[size];
        }
    };
}
