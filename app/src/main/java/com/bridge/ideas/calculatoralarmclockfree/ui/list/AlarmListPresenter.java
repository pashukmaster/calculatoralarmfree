package com.bridge.ideas.calculatoralarmclockfree.ui.list;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BasePresenter;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.helper.HelperAlarm;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.models.ListAlarms;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import static com.bridge.ideas.calculatoralarmclockfree.ui.calculator.CalculatorPresenter.LIST_ALARMS;

@InjectViewState
public class AlarmListPresenter extends BasePresenter<AlarmListView> {
    @Inject LocalStorage localStorage;
    List<Alarm> alarmList;

    AlarmListPresenter() {
        App.getAppComponent().inject(this);
    }

    void showAlarmList() {
        ListAlarms alarms = localStorage.readObject(LIST_ALARMS, ListAlarms.class);
        if (alarms == null) {
            alarms = new ListAlarms();
        }
        alarmList = alarms.getAlarmList();
        Collections.sort(alarmList, new ListAlarms());

        Log.w("PashaShowAlarmList", alarmList.toString());
        getViewState().showAlarmList(alarmList);
    }

    public List<Alarm> getAlarmList() {
        ListAlarms alarms = localStorage.readObject(LIST_ALARMS, ListAlarms.class);
        if (alarms == null) {
            alarms = new ListAlarms();
        }
        Log.d("pasha2", alarms.toString());
        alarmList = alarms.bigConvert(alarmList);
        Log.d("pasha23", alarmList.toString());

        return alarmList;
    }

    void editAlarm() {
        int counter = 0;
        for (int i = 0; i < alarmList.size(); i++) {
            if (alarmList.get(i).getChecked()) {
                counter++;
            }
        }
        if (counter > 1 || counter == 0) {
            getViewState().showToast(R.string.check_one_alarm);
            return;
        }

        for (int i = 0; i < alarmList.size(); i++) {
            if(alarmList.get(i).getChecked()){
                getViewState().editAlarm(alarmList.get(i));
            }
        }
    }

    public Alarm getSingleAlarm (int index) {
        //Collections.sort(alarmList, new ListAlarms());
        Alarm alarm = alarmList.get(alarmList.size() - index - 1); //0
        Log.d("PashaAlarmGet", String.valueOf(index));

        return alarm;
    }

    void removeSelectedFromList() {
        for (int j = 0; j < alarmList.size(); j++) {
            Alarm alarm = alarmList.get(j);
            if (alarm.getChecked()) {
                HelperAlarm.updateAlarm(alarm, false);
                alarmList.remove(j);
                j--;
            }
        }
        ListAlarms listAlarms = new ListAlarms();
        listAlarms.setAlarmList(alarmList);
        localStorage.writeObject(LIST_ALARMS, listAlarms);
        getViewState().showAlarmList(alarmList);
    }

    void reloadAlarms() {
        for (int j = 0; j < alarmList.size(); j++) {
            Alarm alarm = alarmList.get(j);
            HelperAlarm.updateAlarm(alarm, alarm.getEnabled());
        }
        ListAlarms listAlarms = new ListAlarms();
        listAlarms.setAlarmList(alarmList);
        localStorage.writeObject(LIST_ALARMS, listAlarms);
    }

}
