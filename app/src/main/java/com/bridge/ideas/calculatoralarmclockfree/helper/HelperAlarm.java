package com.bridge.ideas.calculatoralarmclockfree.helper;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.models.Parcelables;
import com.bridge.ideas.calculatoralarmclockfree.receivers.AlarmReceiver;

import java.util.Calendar;

public class HelperAlarm {
    public static long updateAlarm(Alarm alarm, boolean set) {
        long firstAlarm = 1639534528996l;
        if (alarm.getRepeat().size() == 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarm.getTime());
            if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
                calendar.add(Calendar.DAY_OF_YEAR, 1);
            }
            Intent myIntent = new Intent(App.getInstance().getApplicationContext(), AlarmReceiver.class);
            final Bundle bundle = new Bundle();
            bundle.putByteArray(Alarm.TAG, Parcelables.toByteArray(alarm));
            myIntent.putExtras(bundle);

            AlarmManager alarmManager = (AlarmManager) App.getInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(App.getInstance().getApplicationContext(), (int) calendar.getTimeInMillis(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            if (set) {
                //alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pendingIntent), pendingIntent);
                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

            } else {
                alarmManager.cancel(pendingIntent);
            }

            firstAlarm = calendar.getTimeInMillis();
        } else {
            for (int i = 0; i < alarm.getRepeat().size(); i++) {
                for (int j = 0; j < 5; j++) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(alarm.getTime());
                    calendar.set(Calendar.DAY_OF_WEEK, alarm.getRepeat().get(i));
                    if (calendar.getTimeInMillis() < System.currentTimeMillis()) {
                        calendar.add(Calendar.WEEK_OF_YEAR, 1);

                    }
                    calendar.add(Calendar.WEEK_OF_YEAR, j);
                    Intent myIntent = new Intent(App.getInstance().getApplicationContext(), AlarmReceiver.class);

                    final Bundle bundle = new Bundle();
                    bundle.putByteArray(Alarm.TAG, Parcelables.toByteArray(alarm));
                    myIntent.putExtras(bundle);

                    AlarmManager alarmManager = (AlarmManager) App.getInstance().getApplicationContext().getSystemService(Context.ALARM_SERVICE);
                    PendingIntent pendingIntent = PendingIntent.getBroadcast(App.getInstance().getApplicationContext(), (int) calendar.getTimeInMillis(), myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                    if (set) {
//                        alarmManager.setAlarmClock(new AlarmManager.AlarmClockInfo(calendar.getTimeInMillis(), pendingIntent), pendingIntent);
                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
                    } else {
                        alarmManager.cancel(pendingIntent);
                    }
                    firstAlarm = firstAlarm < calendar.getTimeInMillis() ? firstAlarm : calendar.getTimeInMillis();
                }

            }
        }
        return firstAlarm;
    }

}
