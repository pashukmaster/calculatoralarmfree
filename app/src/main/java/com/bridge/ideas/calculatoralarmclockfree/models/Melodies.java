package com.bridge.ideas.calculatoralarmclockfree.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Melodies {
    @SerializedName("melody_list")
    List<Melody> melodyList;

    public List<Melody> getMelodyList() {
        if(melodyList == null){
            melodyList = new ArrayList<>();
        }
        return melodyList;
    }

    public void setMelodyList(List<Melody> melodyList) {
        this.melodyList = melodyList;
    }
}
