package com.bridge.ideas.calculatoralarmclockfree.models;

import android.os.Parcel;
import android.os.Parcelable;

public class Parcelables {
    public static byte[] toByteArray(Parcelable parcelable) {
        Parcel parcel = Parcel.obtain();

        parcelable.writeToParcel(parcel, 0);

        byte[] result = parcel.marshall();

        parcel.recycle();

        return (result);
    }

    public static Alarm toParcelableAlarm(byte[] bytes) {

        return toParcelable(bytes, Alarm.CREATOR);
    }

    private static <T> T toParcelable(byte[] bytes, Parcelable.Creator<T> creator) {
        final Parcel parcel = Parcel.obtain();

        parcel.unmarshall(bytes, 0, bytes.length);
        parcel.setDataPosition(0);

        final T result = creator.createFromParcel(parcel);

        parcel.recycle();

        return (result);
    }
}
