package com.bridge.ideas.calculatoralarmclockfree.helper;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

public class FileHelper {
    public static String getRealPathFromURI(Context context, Uri contentUri) {
        if(contentUri.toString().startsWith("content://com.google.android.apps.photos.content")){
            try {
                InputStream is = context.getContentResolver().openInputStream(contentUri);

                Bitmap pictureBitmap = BitmapFactory.decodeStream(is);

                File directory = new File(Environment.getExternalStorageDirectory() + "/Masteram");
                if(!directory.exists()){
                    directory.mkdirs();
                }
                File file = new File(directory.getPath() + "/uploadFile.jpg");

                OutputStream fOut = new FileOutputStream(file);

                pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.flush();
                fOut.close();
                return file.getAbsolutePath();
            } catch (Exception ignored) {
                return null;
            }
        } else {
            Cursor cursor = null;
            try {
                String[] proj = {MediaStore.Images.Media.DATA};
                cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
                if (cursor == null) {
                    return null;
                }
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
    }
}
