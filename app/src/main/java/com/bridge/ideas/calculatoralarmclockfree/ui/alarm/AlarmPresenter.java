package com.bridge.ideas.calculatoralarmclockfree.ui.alarm;

import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.base.BasePresenter;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.models.ListAlarms;
import com.bridge.ideas.calculatoralarmclockfree.ui.main.MainActivity;

import java.util.List;

import javax.inject.Inject;

import static com.bridge.ideas.calculatoralarmclockfree.ui.main.MainPresenter.DELAY;
import static com.bridge.ideas.calculatoralarmclockfree.ui.main.MainPresenter.VIBRATION;

@InjectViewState
public class AlarmPresenter extends BasePresenter<AlarmView> {

    @Inject LocalStorage localStorage;
    List<Alarm> alarmList;

    public static final String LIST_ALARMS = "alarm_list";
    public static final String PUSH_BUTTON_COLOR = "PUSH_BUTTON_COLOR";
    public static final String ALARM_VOLUME = "ALARM_VOLUME";


    public AlarmPresenter() {
        App.getAppComponent().inject(this);
    }

    public void loadAlarm(){
        if(localStorage.readBoolean(DELAY, true)){
            getViewState().showDelay();
        }

        if(localStorage.readBoolean(VIBRATION, true)){
            getViewState().vibrate();
        }
    }

    public Integer getAlarmVolime() {
        Integer alarmVolume = localStorage.readInteger(ALARM_VOLUME);
        return alarmVolume;
    }


    public int removeOnceAlarmWhichAccured(Alarm alarm) {
        ListAlarms alarms = localStorage.readObject(LIST_ALARMS, ListAlarms.class);
        if (alarms == null) {
            alarms = new ListAlarms();
        }
        alarmList = alarms.getAlarmList();
        if(!alarmList.isEmpty()) {
            for (int i = 0; i < alarmList.size(); i++) {
                if (alarmList.get(i).getTime().equals(alarm.getTime()) && alarmList.get(i).getRepeat().isEmpty()) {
                    alarmList.get(i).setChecked(false);
                    alarmList.get(i).setEnabled(false);
                }
            }
        }

        ListAlarms listAlarms = new ListAlarms();
        listAlarms.setAlarmList(alarmList);
        localStorage.writeObject(LIST_ALARMS, listAlarms);

        return 1;
    }




}
