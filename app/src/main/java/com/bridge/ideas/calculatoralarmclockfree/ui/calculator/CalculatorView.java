package com.bridge.ideas.calculatoralarmclockfree.ui.calculator;

import com.bridge.ideas.calculatoralarmclockfree.base.BaseView;

public interface CalculatorView extends BaseView {
    void removeAlarm();
    void openList();
    void setQuickTimes(String one, String two, String three);
}
