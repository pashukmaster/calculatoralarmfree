package com.bridge.ideas.calculatoralarmclockfree.ui.main;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BaseActivity;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;
import com.bridge.ideas.calculatoralarmclockfree.ui.calculator.CalculatorFragment;
import com.bridge.ideas.calculatoralarmclockfree.ui.list.AlarmListFragment;
import com.bridge.ideas.calculatoralarmclockfree.ui.list_melody.MusicListFragment;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.pes.androidmaterialcolorpickerdialog.ColorPicker;
import com.pes.androidmaterialcolorpickerdialog.ColorPickerCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends BaseActivity implements MainView, AlarmListFragment.FragmentListener {

    private static int WAIT_TIME = 60*1000; //1 minute
    public static int DELAY_TIME = 10*60*1000; // delay between iterations: 10min
    public static String UPDATE_TIME_KEY = "update_time_key";

    public static Integer colorPushButton;
    public static Integer progressAlarmVolume = 100;


    public static final String FRAGMENT_MELODIES = "FRAGMENT_MELODIES";
    private static final String FRAGMENT_CALCULATOR = "CalculatorFragment";
    private static final String FRAGMENT_ALARM_LIST = "AlarmListFragment";
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawer;
    public ActionBarDrawerToggle toggle;

    @BindView(R.id.itemVibration) View itemVibration;
    @BindView(R.id.itemPushButtonColorPicker) View itemPushButtonColorPicker;
    @BindView(R.id.iconPushButtonColorPicker) ImageView iconPushButtonColorPicker;

    @BindView(R.id.iconVibration) ImageView iconVibration;
//    @BindView(R.id.iconMusic) ImageView iconMusic;
    @BindView(R.id.iconBlockAlarm) ImageView iconBlockAlarm;
    @BindView(R.id.iconDelay) ImageView iconDelay;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.bridge_ideas) TextView bridge_ideas;
    Menu menu;

    @InjectPresenter MainPresenter mPresenter;

    CalculatorFragment calculatorFragment = new CalculatorFragment();

    Melody melody;
    private InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private SeekBar volumeSeekbar = null;
    private AudioManager audioManager = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        unbinder = ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toggle = new ActionBarDrawerToggle(this, mDrawer, null, R.string.drawer_open, R.string.drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        toggle.setHomeAsUpIndicator(R.drawable.drawer);
        mDrawer.addDrawerListener(toggle);
        toggle.setDrawerSlideAnimationEnabled(true);
        toggle.syncState();


        changeFragment(calculatorFragment, FRAGMENT_CALCULATOR);

        mPresenter.loadData();

        MobileAds.initialize(this, "ca-app-pub-5778778247445542~2446633621");

        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-5778778247445542/8245755215");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        AppEventsLogger.activateApp(getApplication());
        initControls();

    }

    @Override
    public void onRecyclerViewItemClicked(int position) {
        // Handle the RecyclerView item click here
        switch(position) {
            case 0:
                Log.d("Pasha333", "dva");
                break;
            default:
                Log.d("Pasha333", "dva");
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                if(!calculatorFragment.isInEditMode()) {
                    calculatorFragment.editMode(true);
                } else {
                    calculatorFragment.editMode(false);
                }
                break;
            case R.id.share:
                Intent share = new Intent(android.content.Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_SUBJECT, "My alarm time: " + calculatorFragment.getTime());
                share.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.bridge.ideas.calculatoralarmclockfree");
                startActivity(Intent.createChooser(share, "Share your alarm!"));
                break;
            default:
                mDrawer.openDrawer(Gravity.LEFT);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (menu != null) menu.clear();
        getMenuInflater().inflate(R.menu.main_menu, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);

    }

    @OnClick(R.id.iconDrawer)
    protected void clickIconDrawer() {
        mDrawer.closeDrawer(Gravity.LEFT);
    }


    @OnClick(R.id.itemVibration)
    protected void onClickVibration() {
        mPresenter.changeVibration(!iconVibration.isSelected());
    }

    @OnClick(R.id.itemMain)
    protected void onClickMain() {
        calculatorFragment.clearFragment();
        calculatorFragment.editMode(false);
        changeFragment(calculatorFragment, FRAGMENT_CALCULATOR);
        calculatorFragment.setAlarm(null);
    }

    @OnClick(R.id.bridge_ideas)
    protected void onClickBridgeIdeas() {
        //Log.w("Pasha", getApplicationContext().getPackageName().toString());
        Uri uri = Uri.parse("market://details?id=com.bridge.ideas.calculatoralarmclock");
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=com.bridge.ideas.calculatoralarmclock")));
        }
    }

    @OnClick(R.id.itemAlarmList)
    protected void onClickAlarmList() {
        changeFragment(new AlarmListFragment(), FRAGMENT_ALARM_LIST);
    }

//
    @OnClick(R.id.itemBlockAlarm)
    protected void onClickChangeAlarm() {
        changeFragment(calculatorFragment, FRAGMENT_CALCULATOR);
        calculatorFragment.editMode(true);
        calculatorFragment.setAlarm(null);
    }

    public void editAlarm(Alarm alarm) {
        changeFragment(calculatorFragment, FRAGMENT_CALCULATOR);
        calculatorFragment.setAlarm(alarm);
    }

//    @OnClick(R.id.itemMusic)
//    protected void onClickItemMusic() {
//        if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
//        } else {
//            openSelectMelody();
//        }
//    }

private void initControls()
{
    try
    {
        volumeSeekbar = (SeekBar)findViewById(R.id.simpleSeekBar);
        volumeSeekbar.getProgress();
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        volumeSeekbar.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volumeSeekbar.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));


        volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onStopTrackingTouch(SeekBar arg0)
            {
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0)
            {
                mPresenter.getAlarmVolume();
            }

            @Override
            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
            {
                setColorProgressAlarmVolume(progress);
            }
        });
        if(mPresenter.getAlarmVolume() != null) {
            volumeSeekbar.setProgress(mPresenter.getAlarmVolume());
        }
    }
    catch (Exception e)
    {
        e.printStackTrace();
    }
}
    void openSelectMelody() {
        Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, 0);
    }

    public void showVideoAdd() {
        mInterstitialAd.show();
    }

    @OnClick(R.id.itemDelay)
    protected void onClickDelay() {
        mPresenter.changeDelay(!iconDelay.isSelected());
    }

    /*v1.2*/

    public Integer getColorPushButton() {
        Integer colorPushButton = mPresenter.getColorNum();
        if(colorPushButton == null) {
            colorPushButton = 0xe0c7d6e6;
        }
        return colorPushButton;
    }

    public Integer getColorProgressAlarmVolume() {
        Integer alarmVolume = mPresenter.getAlarmVolume();
        if(alarmVolume == null) {
            alarmVolume = 100;
        }
        return alarmVolume;
    }

    public static void setColorPushButton(Integer colorPushButton) {
        MainActivity.colorPushButton = colorPushButton;
    }

    public void setColorProgressAlarmVolume(Integer progressAlarmVolume) {
        mPresenter.setAlarmVolume(progressAlarmVolume);
        MainActivity.progressAlarmVolume = progressAlarmVolume;
    }

    @OnClick(R.id.itemPushButtonColorPicker)
    protected void onClickPushButtonColorPicker() {
        /* Show color picker dialog */
        Integer savedPushButtonColor = mPresenter.getColorNum();
        final ColorPicker cp;
        if(savedPushButtonColor == null) {
            cp = new ColorPicker(MainActivity.this, 0, 0, 0, 0);
        } else {
            cp = new ColorPicker(MainActivity.this, Color.alpha(savedPushButtonColor), Color.red(savedPushButtonColor),
                    Color.green(savedPushButtonColor), Color.blue(savedPushButtonColor));
        }

        cp.show();

        cp.enableAutoClose(); // Enable auto-dismiss for the dialog

        /* Set a new Listener called when user click "select" */
        cp.setCallback(new ColorPickerCallback() {
            @Override
            public void onColorChosen(@ColorInt int color) {
                // Do whatever you want
                // Examples
                Log.d("Alpha", Integer.toString(Color.alpha(color)));
                Log.d("Red", Integer.toString(Color.red(color)));
                Log.d("Green", Integer.toString(Color.green(color)));
                Log.d("Blue", Integer.toString(Color.blue(color)));

                Log.d("Pure Hex", Integer.toHexString(color));

                //colorPushButton = color;
                setColorPushButton(color);
                Log.d("PashaColorChosen", Integer.toString(color));
                mPresenter.setColorNum(color);

                Log.d("#Hex no alpha", String.format("#%06X", (0xFFFFFF & color)));
                Log.d("#Hex with alpha", String.format("#%08X", (0xFFFFFFFF & color)));

                // If the auto-dismiss option is not enable (disabled as default) you have to manually dimiss the dialog
                // cp.dismiss();
            }
        });
    }

//    @OnClick(R.id.itemQuestions)
//    protected void onClickQuestions() {
//
//    }

    @OnClick(R.id.itemBlockAlarm)
    protected void onClickItemBlock() {
        iconBlockAlarm.setSelected(!iconBlockAlarm.isSelected());
    }

    public void changeFragment(Fragment fragment, String tag) {
        if (menu != null)
            menu.setGroupVisible(R.id.main_menu_group, tag.equals(FRAGMENT_CALCULATOR));

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        if (fm.findFragmentByTag(FRAGMENT_MELODIES) != null) {
            ft.hide(fm.findFragmentByTag(FRAGMENT_MELODIES));
        }
        if (fm.findFragmentByTag(FRAGMENT_CALCULATOR) != null) {
            ft.hide(fm.findFragmentByTag(FRAGMENT_CALCULATOR));
        }
        if (fm.findFragmentByTag(FRAGMENT_ALARM_LIST) != null) {
            ft.hide(fm.findFragmentByTag(FRAGMENT_ALARM_LIST));
        }


        Fragment fragmentOld = fm.findFragmentByTag(tag);
        if (fragmentOld != null) {
            fragment = fragmentOld;
            ft.show(fragment).commit();
        } else {
            ft.add(R.id.fragment_container, fragment, tag).show(fragment).commit();
        }
        mDrawer.closeDrawer(Gravity.LEFT);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            openSelectMelody();
        }

    }

    @Override
    public void onBackPressed() {

        Fragment melodiesFragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_MELODIES);
        Fragment alarmListFragment = getSupportFragmentManager().findFragmentByTag(FRAGMENT_ALARM_LIST);

        if (melodiesFragment != null && melodiesFragment.isVisible()) {
            changeFragment(calculatorFragment, FRAGMENT_CALCULATOR);
        } else if(alarmListFragment != null && alarmListFragment.isVisible()) {
            changeFragment(calculatorFragment, FRAGMENT_CALCULATOR);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void showVibration(boolean status) {
        iconVibration.setSelected(status);
    }

    @Override
    public void showColorNumber(int colorNum) {

    }

    @Override
    public void showDelay(boolean status) {
        iconDelay.setSelected(status);
    }

    public void openMelody() {
        changeFragment(new MusicListFragment(), FRAGMENT_MELODIES);
    }

    public void openAlarmList() {
        changeFragment(new AlarmListFragment(), FRAGMENT_ALARM_LIST);
    }


    public Melody getMelody() {
        return melody;
    }

    public void setMelody(Melody melody) {
        calculatorFragment.setMelody(melody);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Uri uri = data.getData();
            mPresenter.addMelody(uri);
            changeFragment(new MusicListFragment(), FRAGMENT_MELODIES);
        }

    }



}
