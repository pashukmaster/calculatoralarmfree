package com.bridge.ideas.calculatoralarmclockfree.receivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.ui.alarm.AlarmActivity;


public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentNew = new Intent(App.getInstance().getApplicationContext(), AlarmActivity.class);

        final Bundle bundle = intent.getExtras();
        intentNew.putExtra(Alarm.TAG, bundle.getByteArray(Alarm.TAG));
        intentNew.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP); // | Intent.FLAG_ACTIVITY_NEW_TASK

        context.startActivity(intentNew);
    }
}