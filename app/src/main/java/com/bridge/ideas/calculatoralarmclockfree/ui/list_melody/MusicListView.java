package com.bridge.ideas.calculatoralarmclockfree.ui.list_melody;

import com.bridge.ideas.calculatoralarmclockfree.base.BaseView;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;

import java.util.List;

public interface MusicListView extends BaseView {

    void showMelodyList(List<Melody> melodyList);
}
