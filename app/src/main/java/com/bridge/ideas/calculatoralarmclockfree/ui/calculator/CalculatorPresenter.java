package com.bridge.ideas.calculatoralarmclockfree.ui.calculator;

import android.app.AlarmManager;
import android.util.Log;

import com.arellomobile.mvp.InjectViewState;
import com.bridge.ideas.calculatoralarmclockfree.App;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BasePresenter;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.helper.HelperAlarm;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.models.ListAlarms;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;

import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;


@InjectViewState
public class CalculatorPresenter extends BasePresenter<CalculatorView> {

    @Inject LocalStorage localStorage;
    List<Alarm> alarmList;

    public static final String LIST_ALARMS = "alarm_list";
    public static final String PUSH_BUTTON_COLOR = "PUSH_BUTTON_COLOR";

    private Melody mMelody;

    CalculatorPresenter() {
        App.getAppComponent().inject(this);
    }


    void saveAlarm(long time, String comment, List<Integer> daysList) {
        Log.d("PashaTest", "saveAlarm");

        ListAlarms alarms = localStorage.readObject(LIST_ALARMS, ListAlarms.class);
        if (alarms == null) {
            alarms = new ListAlarms();
        }

        if (mMelody == null) {
            mMelody = new Melody(R.raw.today_will_be_a_holiday, "Today will be a holiday", null);
        }

        Alarm alarm = new Alarm();
        alarm.setMelody(mMelody);
        alarm.setComment(comment);
        alarm.setId(System.currentTimeMillis());
        alarm.setTime(time);
        alarm.setRepeat(daysList);
        alarm.setEnabled(true);

        int index = findCopyAlarm(alarms.getAlarmList(), alarm);
        if (index != -1) {
            HelperAlarm.updateAlarm(alarms.getAlarmList().get(index), false);
            alarms.getAlarmList().remove(index);
        }
        alarms.getAlarmList().add(alarm);
        localStorage.writeObject(LIST_ALARMS, alarms);
        long firstAlarm = HelperAlarm.updateAlarm(alarm, true);
        getViewState().openList();
        getViewState().showToast(delayToTime(firstAlarm - System.currentTimeMillis()));
    }

    public void setQuickTimes() {
        ListAlarms alarms = localStorage.readObject(LIST_ALARMS, ListAlarms.class);
        if (alarms == null) {
            alarms = new ListAlarms();
        }
        String one = "07:00";
        String two = "08:00";
        String three = "09:00";
        if (alarms.getAlarmList().size() > 0) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarms.getAlarmList().get(alarms.getAlarmList().size() - 1).getTime());
            one = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        }

        if (alarms.getAlarmList().size() > 1) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarms.getAlarmList().get(alarms.getAlarmList().size() - 2).getTime());
            two = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        }

        if (alarms.getAlarmList().size() > 2) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarms.getAlarmList().get(alarms.getAlarmList().size() - 3).getTime());
            three = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        }

        getViewState().setQuickTimes(one, two, three);

    }

    private int findCopyAlarm(List<Alarm> alarms, Alarm alarm) {
        for (int i = 0; i < alarms.size(); i++) {
            Calendar calendar1 = Calendar.getInstance();
            Calendar calendar2 = Calendar.getInstance();
            calendar1.setTimeInMillis(alarms.get(i).getTime());
            calendar2.setTimeInMillis(alarm.getTime());

            if (calendar1.get(Calendar.MINUTE) == calendar2.get(Calendar.MINUTE) && calendar1.get(Calendar.HOUR_OF_DAY) == calendar2.get(Calendar.HOUR_OF_DAY)) {
                if (alarms.get(i).getRepeat().size() == alarm.getRepeat().size()) {
                    boolean equals = true;
                    for (int j = 0; j < alarms.get(i).getRepeat().size(); j++) {
                        if (!alarms.get(i).getRepeat().get(j).equals(alarm.getRepeat().get(j))) {
                            equals = false;
                        }
                    }
                    if (equals) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    private String delayToTime(long delay) {
        if (delay < 60000) {
            return App.getInstance().getApplicationContext().getResources().getString(R.string.alarm_will_be_less_a_minute);
        }
        String result = App.getInstance().getApplicationContext().getResources().getString(R.string.alarm_will_be_in);
        int days = (int) (delay / (AlarmManager.INTERVAL_DAY));
        int hours = (int) ((delay - (days * AlarmManager.INTERVAL_DAY)) / AlarmManager.INTERVAL_HOUR);
        int minutes = (int) ((delay - (days * AlarmManager.INTERVAL_DAY) - (hours * AlarmManager.INTERVAL_HOUR)) / 60000);
        if (days > 0) {
            result = result + " " + days + " " + App.getInstance().getApplicationContext().getResources().getString(R.string.days);
        }

        if (hours > 0) {
            result = result + " " + hours + " " + App.getInstance().getApplicationContext().getResources().getString(R.string.hours);
        }

        if (minutes > 0) {
            result = result + " " + minutes + " " + App.getInstance().getApplicationContext().getResources().getString(R.string.minutes);
        }
        return result;
    }

    void editAlarm(Alarm alarm, long time, String comment, List<Integer> daysList) {
        HelperAlarm.updateAlarm(alarm, false);
        ListAlarms alarms = localStorage.readObject(LIST_ALARMS, ListAlarms.class);
        alarms.removeAlarm(alarm);
        localStorage.writeObject(LIST_ALARMS, alarms);
        saveAlarm(time, comment, daysList);
        getViewState().removeAlarm();
    }

    public void setMelody(Melody melody) {
        mMelody = melody;
    }

    public int showColorNum() {
        Integer pushButtonColor = localStorage.readInteger(PUSH_BUTTON_COLOR);
        return pushButtonColor;
    }

    public int showAlarmList() {
        ListAlarms alarms = localStorage.readObject(LIST_ALARMS, ListAlarms.class);
        if (alarms == null) {
            alarms = new ListAlarms();
        }

        alarmList = alarms.getAlarmList();
        Integer am = 0;
        if(!alarmList.isEmpty()) {
            am = alarms.getClosestAlarmWhichIsOnInMins(alarmList);
        } else {
            am = -1;
        }


        return am;
    }
}
