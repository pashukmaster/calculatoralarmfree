package com.bridge.ideas.calculatoralarmclockfree.ui.list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BaseRecyclerAdapter;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;

import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlarmAdapter extends BaseRecyclerAdapter {
    private List<Alarm> mAlarms;
    private IClickListener listener;
    private AlarmListFragment alarmListFragment = new AlarmListFragment();

    AlarmAdapter(List<Alarm> alarms) {
        mAlarms = alarms;
    }


    @Override
    public int getItemCount() {
        return mAlarms.size();
    }

    @Override
    public Alarm getItem(int position) {
        return mAlarms.get(mAlarms.size() - position - 1);
    }

    @Override
    public long getItemId(int position) {
        return mAlarms.size() - position - 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AlarmAdapter.ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_alarm, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((AlarmAdapter.ViewHolder) holder).bindView(getItem(position));
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.repeat) TextView repeat;
        @BindView(R.id.time) TextView time;
        @BindView(R.id.enabled) Switch enabled;
        @BindView(R.id.comment) TextView comment;
        @BindView(R.id.checkbox) CheckBox checkbox;


        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }


        void bindView(Alarm alarm) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(alarm.getTime());
            time.setText(String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" +String.format("%02d", calendar.get(Calendar.MINUTE)));
            if (alarm.getRepeat().size() == 7) {
                repeat.setText(R.string.everyday);
            } else if (alarm.getRepeat().size() == 0) {
                repeat.setText(R.string.once);
            } else {
                StringBuilder strRepeat = new StringBuilder();
                for (int i = 0; i < alarm.getRepeat().size(); i++) {
                    switch (alarm.getRepeat().get(i)) {
                        case Calendar.MONDAY:
                            strRepeat.append(repeat.getContext().getResources().getString(R.string.mon)).append(",");
                            break;
                        case Calendar.TUESDAY:
                            strRepeat.append(repeat.getContext().getResources().getString(R.string.tue)).append(",");
                            break;
                        case Calendar.WEDNESDAY:
                            strRepeat.append(repeat.getContext().getResources().getString(R.string.wed)).append(",");
                            break;
                        case Calendar.THURSDAY:
                            strRepeat.append(repeat.getContext().getResources().getString(R.string.thu)).append(",");
                            break;
                        case Calendar.FRIDAY:
                            strRepeat.append(repeat.getContext().getResources().getString(R.string.fri)).append(",");
                            break;
                        case Calendar.SATURDAY:
                            strRepeat.append(repeat.getContext().getResources().getString(R.string.sat)).append(",");
                            break;
                        case Calendar.SUNDAY:
                            strRepeat.append(repeat.getContext().getResources().getString(R.string.sun)).append(",");
                            break;
                    }
                }
                strRepeat.deleteCharAt(strRepeat.length() - 1);
                repeat.setText(strRepeat);
            }
            checkbox.setChecked(alarm.getChecked());
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    alarm.setChecked(isChecked);
                }
            });

            enabled.setChecked(alarm.getEnabled());
            Log.w("Pasha", alarm.getEnabled().toString());
            enabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    alarm.setEnabled(isChecked);
                    Log.d("Pashaalarm.setEnabled", String.valueOf(alarm));
                    listener.onChangeEnabled();

                }
            });

            comment.setVisibility(TextUtils.isEmpty(alarm.getComment()) ? View.GONE : View.VISIBLE);
            comment.setText(alarm.getComment());

            repeat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("PashaAlarm33", String.valueOf(alarm.getTime()));
                    //alarmListFragment.editAlarm(alarm);
                    if(listener != null) {
                        Log.d("PashaAlarm33list", "list");
                        Log.d("PashaAlarm33list", String.valueOf((getAdapterPosition())));

                        listener.onItemClicked(getAdapterPosition());
                    }
                }
            });
//
            time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("PashaAlarm34", String.valueOf(alarm.getTime()));
                    //alarmListFragment.editAlarm(alarm);
                    if(listener != null) {
                        listener.onItemClicked(getAdapterPosition());
                    }
                }
            });

            comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("PashaAlarm3", String.valueOf(alarm.getTime()));
                    //alarmListFragment.editAlarm(alarm);
                    if(listener != null) {
                        listener.onItemClicked(getAdapterPosition());
                    }
                }
            });
        }
    }

    public void setListener(IClickListener listener) {
        this.listener = listener;
    }

    public interface IClickListener {
        void onChangeEnabled();
        void onItemClicked(int position);
    }
}