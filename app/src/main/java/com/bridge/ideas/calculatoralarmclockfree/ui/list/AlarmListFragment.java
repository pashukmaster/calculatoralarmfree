package com.bridge.ideas.calculatoralarmclockfree.ui.list;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BaseFragment;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.ui.main.MainActivity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AlarmListFragment extends BaseFragment implements AlarmListView {
    private FragmentListener mListener;

    public interface FragmentListener {
        void onRecyclerViewItemClicked(int position);
    }

    @BindView(R.id.list) RecyclerView list;
    AlarmAdapter alarmAdapter;

    @Inject LocalStorage localStorage;
    List<Alarm> alarmList;
    @InjectPresenter AlarmListPresenter mPresenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alarm_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initRecyclerView();

        mPresenter.showAlarmList();
    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        Log.w("PashaOnHiddenChanged", "Do it");
        if (!hidden)
            mPresenter.showAlarmList();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        Log.w("Pasha setMenuVisibility", "setMenuVisibility");

        if (visible) {
            // ...
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            FragmentListener mCallback = (FragmentListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement FragmentListener");
        }
    }



    @Override
    public void setUserVisibleHint(boolean visible)
    {
        super.setUserVisibleHint(visible);
        if (visible && isResumed())
        {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();
        if (!getUserVisibleHint())
        {
            return;
        }
        mPresenter.showAlarmList();

        Log.w("PashaResume", "setMenuVisibility2");

        //INSERT CUSTOM CODE HERE
    }

    private void initRecyclerView() {
        list.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        list.setLayoutManager(llm);
    }

    @Override
    public void showAlarmList(List<Alarm> alarmList) {
        alarmList = mPresenter.getAlarmList();

        if (alarmList.size() > 0) {
            Collections.sort(alarmList, new Comparator<Alarm>() {
                @Override
                public int compare(final Alarm object1, final Alarm object2) {
                    return object1.getTime().compareTo(object2.getTime());
                }
            });
        }

        alarmAdapter = new AlarmAdapter(alarmList);
        alarmAdapter.setListener(new AlarmAdapter.IClickListener() {
            @Override
            public void onChangeEnabled() {
                mPresenter.reloadAlarms();
            }

            @Override
            public void onItemClicked(int position) {
                Log.d("PashaAlarm33list", "continue");
                if(position >= 0) {
                    Alarm toMove = mPresenter.getSingleAlarm(position);
                    ((MainActivity) getActivity()).editAlarm(toMove);
                }
            }

        });
        list.setAdapter(alarmAdapter);

    }



    @Override
    public void editAlarm(Alarm alarm) {
        ((MainActivity) getActivity()).editAlarm(alarm);
    }

    @OnClick(R.id.trash)
    protected void onClickTrash() {
        mPresenter.removeSelectedFromList();
    }

    @OnClick(R.id.edit)
    protected void onClickEdit() {
        mPresenter.editAlarm();
    }
}
