package com.bridge.ideas.calculatoralarmclockfree;

import android.support.multidex.MultiDexApplication;

import com.bridge.ideas.calculatoralarmclockfree.dagger.AppComponent;
import com.bridge.ideas.calculatoralarmclockfree.dagger.DaggerAppComponent;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.ContextModule;


public class App extends MultiDexApplication {

    private static App sInstance;

    private static AppComponent sAppComponent;


    public static App getInstance() {
        return sInstance;
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    private float dp;

    @Override
    public void onCreate() {
        super.onCreate();
        //  Fabric.with(this, new Crashlytics());
        sInstance = this;

        dp = getResources().getDisplayMetrics().density;

        sAppComponent = DaggerAppComponent.builder().contextModule(new ContextModule(this)).build();

    }

    public float getDp() {
        return dp;
    }
}