package com.bridge.ideas.calculatoralarmclockfree.ui.calculator;


import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bridge.ideas.calculatoralarmclockfree.R;
import com.bridge.ideas.calculatoralarmclockfree.base.BaseFragment;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorage;
import com.bridge.ideas.calculatoralarmclockfree.models.Alarm;
import com.bridge.ideas.calculatoralarmclockfree.models.Melody;
import com.bridge.ideas.calculatoralarmclockfree.ui.main.MainActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import butterknife.OnTextChanged;


public class CalculatorFragment extends BaseFragment implements CalculatorView {

    @InjectPresenter CalculatorPresenter mPresenter;

    @Inject LocalStorage localStorage;
    List<Alarm> alarmList;

    @BindView(R.id.time) TextView time;
    @BindView(R.id.nextTime) TextView nextTime;
    @BindView(R.id.comment) EditText comment;
    @BindView(R.id.plus) TextView plus;
    @BindView(R.id.minus) TextView minus;
    @BindView(R.id.equals) TextView equals;
    @BindView(R.id.buttonBottom) TextView buttonBottom;
    @BindView(R.id.buttonTop) TextView buttonTop;
    @BindView(R.id.melody) TextView melody;
    private Boolean isPlus;
    private String quantity = "";
    private Boolean isHour;

    private Alarm alarm;

    private static Boolean isInEditMode = false;
    private static Boolean allowToSetMelody = false;

    public static Boolean getAllowToSetMelody() {
        return allowToSetMelody;
    }

    public static void setAllowToSetMelody(Boolean allowToSetMelody) {
        CalculatorFragment.allowToSetMelody = allowToSetMelody;
    }

    public static Integer getPushSavedColorNumLocal() {
        return pushSavedColorNumLocal;
    }

    public static void setPushSavedColorNumLocal(Integer pushSavedColorNumLocal) {
        CalculatorFragment.pushSavedColorNumLocal = pushSavedColorNumLocal;
    }

    private static Integer pushSavedColorNumLocal = 0xe0c7d6e6;

    private static final String SETTINGS = "settings";
    public static final String LIST_ALARMS = "alarm_list";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_calculator, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter.setQuickTimes();
        setNextTime();

        Integer pushSavedColorNum = mPresenter.showColorNum();
        Log.d("PashaColor", pushSavedColorNum.toString());
        if(pushSavedColorNum.equals(null)) {
            pushSavedColorNum = 0xe0c7d6e6;
        }

        setPushSavedColorNumLocal(pushSavedColorNum);

        return view;
    }

    public boolean isInEditMode() {
        return isInEditMode;
    }

    @OnLongClick(R.id.time)
    protected boolean onLongClickTime() {
        comment.setEnabled(true);
        comment.requestFocus();
        if (getActivity() != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null)
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        }
        return true;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden){

            if (alarm != null) {
                comment.setText(alarm.getComment());
                if(alarm.getMelody() != null && !getAllowToSetMelody()){
                    Log.d("PashaMelody", "onHiddenChanged");
                    melody.setText(alarm.getMelody().getName());
                }
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(alarm.getTime());
                time.setText(String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE)));
                editMode(false); /*v1.2 change to false*/
                setAllowToSetMelody(false);
            }

        } else {
            editMode(false);
        }
        setNextTime();
    }

    @OnClick(R.id.itemMelody)
    protected void clickItemMelody(){
        ((MainActivity)getActivity()).openMelody();
    }

    public String getTime() {
        return time.getText().toString();
    }

    @OnTextChanged(R.id.comment)
    protected void onTextChangedComment() {
        if (comment.getText().toString().length() > 64) {
            comment.setText(comment.getText().toString().substring(0, 64));
            comment.setSelection(comment.getText().length());
        }
    }

    @OnClick(R.id.refresh)
    protected void onClickRefresh() {
        time.setText("");
        setNextTime();
    }

    @SuppressLint("ResourceType")
    @OnClick(R.id.turn)
    protected void onClickTurn(View view) {
        buttonEffect(view, pushSavedColorNumLocal); /*v1.2*/

        if (time.getText().length() != 5) {
            showToast(R.string.enter_alarm_time);
            return;
        }
        ((MainActivity) Objects.requireNonNull(getActivity())).showVideoAdd();

        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(time.getText().toString().split(":")[0]));
        calendar.set(Calendar.MINUTE, Integer.valueOf(time.getText().toString().split(":")[1]));
        calendar.set(Calendar.SECOND, 0);

        RepeatDialog repeatDialog = new RepeatDialog();
        repeatDialog.setListener(new RepeatDialog.IChooseListener() {
            @Override
            public void onChoose(List<Integer> repeats) {
                if (alarm == null) {
                    mPresenter.saveAlarm(calendar.getTimeInMillis(), comment.getText().toString(), repeats);
                } else {
                    Log.d("PashaTest", "edit");
                    mPresenter.editAlarm(alarm, calendar.getTimeInMillis(), comment.getText().toString(), repeats);
                }

            }
        });
        if (getActivity() != null) {
            repeatDialog.show(getActivity().getSupportFragmentManager(), "repeatDialog");
        }
    }


    @SuppressLint({"DefaultLocale", "SetTextI18n"})
    @OnClick({R.id.number1
            , R.id.number2
            , R.id.number3
            , R.id.number4
            , R.id.number5
            , R.id.number6
            , R.id.number7
            , R.id.number8
            , R.id.number9})
    protected void onClickNumber(View view) {
        buttonEffect(view, pushSavedColorNumLocal); /*v1.2*/
        if(isInEditMode == false) {
            if (time.getText().length() == 5) {
                time.setText("");
            }
        }
        switch (view.getId()) {
            case R.id.number1:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 1;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText("1");
                } else if (time.getText().toString().length() == 1) {
                    time.setText(time.getText().toString() + 1 + ":");
                } else if (time.getText().toString().length() < 5) {
                    time.setText(time.getText().toString() + 1);
                }
                break;
            case R.id.number2:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 2;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText("2");
                } else if (time.getText().toString().length() == 1) {
                    time.setText(time.getText().toString() + 2 + ":");
                } else if (time.getText().toString().length() < 5) {
                    time.setText(time.getText().toString() + 2);
                }
                break;
            case R.id.number3:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 3;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText(String.format("%02d", 3) + ":");
                } else if (time.getText().toString().length() == 1 && (time.getText().toString().equals("0") || time.getText().toString().equals("1") || time.getText().toString().equals("2"))) {
                    time.setText(time.getText().toString() + 3 + ":");
                } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 2) {
                    time.setText(time.getText().toString() + 3);
                }
                break;
            case R.id.number4:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 4;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText(String.format("%02d", 4) + ":");
                } else if (time.getText().toString().length() == 1 && (time.getText().toString().equals("0") || time.getText().toString().equals("1"))) {
                    time.setText(time.getText().toString() + 4 + ":");
                } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 2) {
                    time.setText(time.getText().toString() + 4);
                }
                break;
            case R.id.number5:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 5;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText(String.format("%02d", 5) + ":");
                } else if (time.getText().toString().length() == 1 && (time.getText().toString().equals("0") || time.getText().toString().equals("1"))) {
                    time.setText(time.getText().toString() + 5 + ":");
                } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 2) {
                    time.setText(time.getText().toString() + 5);
                }
                break;
            case R.id.number6:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 6;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText(String.format("%02d", 6) + ":");
                } else if (time.getText().toString().length() == 1 && (time.getText().toString().equals("0") || time.getText().toString().equals("1"))) {
                    time.setText(time.getText().toString() + 6 + ":");
                } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 3) {
                    time.setText(time.getText().toString() + 6);
                }
                break;
            case R.id.number7:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 7;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText(String.format("%02d", 7) + ":");
                } else if (time.getText().toString().length() == 1 && (time.getText().toString().equals("0") || time.getText().toString().equals("1"))) {
                    time.setText(time.getText().toString() + 7 + ":");
                } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 3) {
                    time.setText(time.getText().toString() + 7);
                }
                break;
            case R.id.number8:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 8;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText(String.format("%02d", 8) + ":");
                } else if (time.getText().toString().length() == 1 && (time.getText().toString().equals("0") || time.getText().toString().equals("1"))) {
                    time.setText(time.getText().toString() + 8 + ":");
                } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 3) {
                    time.setText(time.getText().toString() + 8);
                }
                break;
            case R.id.number9:
                if (equals.getText().equals("=") && time.getText().toString().length() == 5) {
                    quantity = quantity + 9;
                    return;
                }
                if (time.getText().toString().length() == 0) {
                    time.setText(String.format("%02d", 9) + ":");
                } else if (time.getText().toString().length() == 1 && (time.getText().toString().equals("0") || time.getText().toString().equals("1"))) {
                    time.setText(time.getText().toString() + 9 + ":");
                } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 3) {
                    time.setText(time.getText().toString() + 9);
                }
                break;
        }
    }


    @SuppressLint("SetTextI18n")
    @OnClick(R.id.buttonBottom)
    protected void onClickButtonBottom() {
       // buttonEffect(view, pushSavedColorNumLocal); /*v1.2*/

        if (buttonBottom.getText().equals("00")) {
            if (time.getText().toString().length() == 0) {
                time.setText("00:");
            } else if (time.getText().toString().length() == 3) {
                time.setText(time.getText().toString() + "00");
            }
        } else {
            isHour = false;
        }
    }

    @SuppressLint("SetTextI18n")
    @OnClick(R.id.buttonTop)
    protected void onClickButtonTop() {
        //buttonEffect(view, pushSavedColorNumLocal); /*v1.2*/
        if (buttonTop.getText().equals("0")) {
            if (time.getText().toString().length() == 0) {
                time.setText("0");
            } else if (time.getText().toString().length() == 1) {
                time.setText(time.getText().toString() + 0 + ":");
            } else if (time.getText().toString().length() < 5 && time.getText().toString().length() > 2) {
                time.setText(time.getText().toString() + 0);
            }
        } else {
            isHour = true;
        }
    }

    @OnClick(R.id.plus)
    protected void onClickPlus(View view) {
        buttonEffect(view, pushSavedColorNumLocal); /*v1.2*/
        if (plus.getText().toString().equals("+")) {
            isPlus = true;
        } else {
            time.setText(plus.getText().toString());
        }
        //   time.setText(time.getText().toString().substring(0, 5) + "+");
    }

    @OnClick(R.id.minus)
    protected void onClickMinus(View view) {
        buttonEffect(view, pushSavedColorNumLocal); /*v1.2*/

        if (minus.getText().toString().equals("-")) {
            isPlus = false;
        } else {
            time.setText(minus.getText().toString());
        }
        //   time.setText(time.getText().toString().substring(0, 5) + "-");
    }

    @OnClick(R.id.equals)
    protected void onClickEquals(View view) {
        buttonEffect(view, pushSavedColorNumLocal); /*v1.2*/


        if (equals.getText().toString().equals("=")) {
        } else {
            time.setText(equals.getText().toString());
        }

        if (isHour == null || isPlus == null) {
            return;
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        try {
            Date date = simpleDateFormat.parse(time.getText().toString());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            if (isHour) {
                cal.add(Calendar.HOUR_OF_DAY, Integer.valueOf(quantity) * (isPlus ? 1 : -1));
            } else {
                cal.add(Calendar.MINUTE, Integer.valueOf(quantity) * (isPlus ? 1 : -1));
            }
            Date newDate = cal.getTime();
            time.setText(simpleDateFormat.format(newDate));
        } catch (Exception e) {
            showToast(e.getMessage());
            isHour = null;
            isPlus = null;
            quantity = "";
        }

        isHour = null;
        isPlus = null;
        quantity = "";
    }

    @SuppressLint({"ResourceType", "SetTextI18n", "DefaultLocale"})
    public void editMode(boolean status) {
        if(status == true) {
            isInEditMode = true;
        } else {
            isInEditMode = false;
        }

        if (time.getText().length() != 5 && !plus.getText().toString().equals("+") && status) {
            showToast(R.string.enter_alarm_time);
            return;
        }
        if (status) {
            minus.setText("-");
            minus.setTextSize(40f);
            plus.setText("+");
            plus.setTextSize(30f);
            equals.setText("=");
            equals.setTextSize(30f);
            buttonBottom.setText(R.string.min);
            buttonBottom.setTextSize(16f);
            buttonTop.setText(R.string.hour);
            buttonTop.setTextSize(16f);
        } else {
            mPresenter.setQuickTimes();
            plus.setTextSize(18f);
            minus.setTextSize(18f);
            equals.setTextSize(18f);
            buttonTop.setText("0");
            buttonTop.setTextSize(30f);
            buttonBottom.setText("00");
            buttonBottom.setTextSize(20f);
        }
    }

    public List<String> getLastEnteredTime() {
        List<String> times = new ArrayList<>();
        times.add("07:00");
        times.add("08:00");
        times.add("09:00");
        return times;
    }

    public void setAlarm(Alarm alarm) {
        this.alarm = alarm;
    }

    @Override
    public void removeAlarm() {
        alarm = null;
    }

    @Override
    public void openList() {
        ((MainActivity) getActivity()).openAlarmList();
    }

    @Override
    public void setQuickTimes(String one, String two, String three) {
        plus.setText(one);
        minus.setText(two);
        equals.setText(three);
    }

    public void setMelody(Melody m){
        setAllowToSetMelody(true);
        melody.setText(m.getName());
        Log.d("PashaMelodyBase", m.getName().toString());
        mPresenter.setMelody(m);
    }

    public void clearFragment(){
        mPresenter.setMelody(null);
        melody.setText(R.string.today_will_be_a_holiday);
        comment.setText("");
        time.setText("");
        melody.setText(R.string.today_will_be_a_holiday);
    }

    /*v1.2*/
    public void buttonEffect(View button, Integer colorNum){

        button.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN: {
                        Integer pushedColor = ((MainActivity) getActivity()).getColorPushButton();
                        Log.d("PashaColorPushedColor", pushedColor.toString());
                        if(pushedColor == 0 || pushedColor == null) {
                            pushedColor = 0xe0c7d6e6;
                        }
                        //pushedColor 0xe0c7d6e6
                        v.getBackground().setColorFilter(pushedColor, PorterDuff.Mode.SRC_ATOP);
                        v.invalidate();
                        break;
                    }
                    case MotionEvent.ACTION_UP: {
                        v.getBackground().clearColorFilter();
                        v.invalidate();
                        break;
                    }
                }
                return false;
            }
        });
    }


    public void setNextTime() {
        Calendar calendar = Calendar.getInstance();

        //12:05 - 7 день недели, 18:30 1,2,3,4,5,6,7
        int nextTimeInMinutes = mPresenter.showAlarmList();
        if(nextTimeInMinutes == -1) {
            nextTime.setText("Next alarm: No alarm");
        }

        int minDayInArray =	nextTimeInMinutes / 1440;

        int minTimeInArray =	nextTimeInMinutes % 1440;
        int hoursMin = minTimeInArray / 60;
        int minutesMin = minTimeInArray % 60;
        Integer dayFormat = 0;

        if(minDayInArray == 0) {
            dayFormat = calendar.get(Calendar.DAY_OF_WEEK);
        } else {
            dayFormat = minDayInArray ;
        }

        String asWeek = "";
        switch (dayFormat) {
            case Calendar.MONDAY:
                asWeek = "Mon";
                break;
            case Calendar.TUESDAY:
                asWeek = "Tue";
                break;
            case Calendar.WEDNESDAY:
                asWeek = "Wed";
                break;
            case Calendar.THURSDAY:
                asWeek = "Thu";
                break;
            case Calendar.FRIDAY:
                asWeek = "Fri";
                break;
            case Calendar.SATURDAY:
                asWeek = "Sat";
                break;
            case Calendar.SUNDAY:
                asWeek = "Sun";
                break;
            default:
               asWeek = "Today";
               break;
        }

        if(hoursMin >= 0 &&  minutesMin >= 0 && dayFormat > 0) {
            String timeFormat = String.format("%02d", hoursMin) + ":" + String.format("%02d", minutesMin);
            nextTime.setText("Next alarm:"  + asWeek + ", " + timeFormat);
        }

    }


}
