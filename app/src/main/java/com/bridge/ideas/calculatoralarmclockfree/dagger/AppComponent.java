package com.bridge.ideas.calculatoralarmclockfree.dagger;

import com.bridge.ideas.calculatoralarmclockfree.dagger.module.ContextModule;
import com.bridge.ideas.calculatoralarmclockfree.dagger.module.LocalStorageModule;
import com.bridge.ideas.calculatoralarmclockfree.ui.alarm.AlarmPresenter;
import com.bridge.ideas.calculatoralarmclockfree.ui.calculator.CalculatorPresenter;
import com.bridge.ideas.calculatoralarmclockfree.ui.list.AlarmListPresenter;
import com.bridge.ideas.calculatoralarmclockfree.ui.list_melody.MusicListPresenter;
import com.bridge.ideas.calculatoralarmclockfree.ui.main.MainPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {ContextModule.class, LocalStorageModule.class})
public interface AppComponent {

    void inject(CalculatorPresenter example);
    void inject(AlarmListPresenter example);
    void inject(MusicListPresenter example);
    void inject(MainPresenter example);
    void inject(AlarmPresenter example);

}

